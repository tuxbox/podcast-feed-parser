package me.sniggle.podcast.feed;

import me.sniggle.podcast.feed.parser.FeedParser;

import java.io.*;
import java.util.concurrent.Future;

/**
 * Created by iulius on 20/06/16.
 */
public class FeedUpdater {

  public static void main(String[] args) {
      try {
        Future<InputStream> future =new FeedDownloader().downloadFeed("http://freakshow.fm/feed/m4a", null, null);
        new FeedParser().parse(future);
      } catch (IOException e) {
          e.printStackTrace();
      }
  }

}
