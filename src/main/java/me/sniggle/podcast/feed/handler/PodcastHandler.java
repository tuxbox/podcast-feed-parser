package me.sniggle.podcast.feed.handler;

import me.sniggle.podcast.feed.model.Podcast;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Created by iulius on 20/06/16.
 */
public class PodcastHandler extends DefaultHandler {

  private Podcast podcast = null;
  private boolean rss = false;
  private boolean channel = false;
  private boolean item = false;
  private boolean image = false;
  private boolean link = false;

  public PodcastHandler(Podcast podcast) {
    super();
    this.podcast = podcast;
  }

  public void startDocument () throws SAXException {
    System.out.println("--- start document ---");
  }

  public void endDocument () throws SAXException {
    System.out.println("--- end document ---");
  }

  public void startElement (String uri, String localName, String qName, Attributes attributes) throws SAXException {
    if( "".equals(uri) ) {
      if("rss".equals(localName)) {
        rss = true;
        if( podcast == null ) {
          podcast = new Podcast();
        }
      }
      if("channel".equals(localName)) {
        channel = true;
      }
      if("item".equals(localName)) {
        item = true;
      }
      if("image".equals(localName)) {
        image = true;
      }
      if("link".equals(localName)) {
        link = true;
      }
      if( "title".equals(localName) ) {
        if( item ) {

        } else if( channel ) {
          podcast.setTitle();
        }
      }
    }
    System.out.println("start --- " + uri+":"+localName+":"+qName);
  }

  public void endElement (String uri, String localName, String qName) throws SAXException {
    System.out.println("end   --- " +uri+":"+localName+":"+qName);
    if( "".equals(uri) ) {
      if("rss".equals(localName)) {
        rss = false;
      }
      if("channel".equals(localName)) {
        channel = false;
      }
      if("item".equals(localName)) {
        item = false;
      }
      if("image".equals(localName)) {
        image = false;
      }
      if("link".equals(localName)) {
        link = false;
      }
    }
  }


}
