package me.sniggle.podcast.feed;

import com.mashape.unirest.http.HttpResponse;

import java.io.InputStream;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by iulius on 20/06/16.
 */
public class RequestInputStreamFuture<T> implements Future<InputStream> {

  private final Future<HttpResponse<InputStream>> future;

  public RequestInputStreamFuture(Future<HttpResponse<InputStream>> unirestAsyncResponse) {
    this.future = unirestAsyncResponse;
  }

  @Override
  public boolean cancel(boolean mayInterruptIfRunning) {
    return future.cancel(mayInterruptIfRunning);
  }

  @Override
  public boolean isCancelled() {
    return future.isCancelled();
  }

  @Override
  public boolean isDone() {
    return future.isDone();
  }

  @Override
  public InputStream get() throws InterruptedException, ExecutionException {
    return future.get().getBody();
  }

  @Override
  public InputStream get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
    return future.get(timeout, unit).getBody();
  }
}
