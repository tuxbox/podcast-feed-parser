package me.sniggle.podcast.feed.model;

import java.time.ZonedDateTime;

/**
 * Created by iulius on 20/06/16.
 */
public class Podcast {

  public static class CoverImage {
    private String url;
    private String title;
    private String link;

    public String getUrl() {
      return url;
    }

    public void setUrl(String url) {
      this.url = url;
    }

    public String getTitle() {
      return title;
    }

    public void setTitle(String title) {
      this.title = title;
    }

    public String getLink() {
      return link;
    }

    public void setLink(String link) {
      this.link = link;
    }
  }

  public static class Link {

  }

  private String title;
  private String link;
  private String description;
  private ZonedDateTime dateTime;
  private CoverImage image;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getLink() {
    return link;
  }

  public void setLink(String link) {
    this.link = link;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public ZonedDateTime getDateTime() {
    return dateTime;
  }

  public void setDateTime(ZonedDateTime dateTime) {
    this.dateTime = dateTime;
  }

  public CoverImage getImage() {
    return image;
  }

  public void setImage(CoverImage image) {
    this.image = image;
  }
}
