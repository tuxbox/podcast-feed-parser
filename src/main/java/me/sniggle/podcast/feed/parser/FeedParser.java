package me.sniggle.podcast.feed.parser;

import com.mashape.unirest.http.HttpResponse;
import me.sniggle.podcast.feed.handler.PodcastHandler;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by iulius on 20/06/16.
 */
public class FeedParser {

  public void parse(Future<InputStream> source) throws IOException {
    try {
      SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
      saxParserFactory.setNamespaceAware(true);
      SAXParser saxParser = saxParserFactory.newSAXParser();
      saxParser.parse(source.get(), new PodcastHandler());
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    } catch (SAXException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    } catch (ExecutionException e) {
      e.printStackTrace();
    }

  }

}
