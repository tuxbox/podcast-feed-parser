package me.sniggle.podcast.feed;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.GetRequest;

import java.io.InputStream;
import java.util.concurrent.Future;

/**
 * Created by iulius on 20/06/16.
 */
public class FeedDownloader {

  public Future<InputStream> downloadFeed(String url, String username, String password) {
    GetRequest request = Unirest.get(url);
    if( username != null && !username.trim().equals("") &&
          password != null && !password.trim().equals("") ) {
      request = request.basicAuth(username, password);
    }
    return new RequestInputStreamFuture<InputStream>(request.asBinaryAsync());
  }

}
